require 'test/unit'
require './source/primeInterface'

class Tc_PrimeCheck < Test::Unit::TestCase
  def setup
      @pc = PrimeCheck.new
      @pl = PrimeList.new
  end  

  def test_checktype
    error = RuntimeError
    assert_raise(error){ @pc.prime?('2') }
    assert_raise(error){ @pl.primes(2,'b') } 
    assert_raise(error){ @pl.primes('z',6) }
  end

  def test_prime
    assert(@pc.prime?(2017))
    assert_equal(false, @pc.prime?(1))
    assert_equal(true, @pc.prime?(11))
    assert_equal(false, @pc.prime?(0))
  end

  def test_primes
    assert_equal([2,3,5],@pl.primes(2,6))
    assert_equal([2,3,5],@pl.primes(6,2))
  end
end