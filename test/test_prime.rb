require 'test/unit'
require './source/prime'

class Tc_PrimeInterface < Test::Unit::TestCase
  def setup
    @p = PrimeInterface.new
  end
  
  def test_checktype
    error = RuntimeError
    assert_raise(error){ @p.prime?('2') }
    assert_raise(error){ @p.primes(2,'b') } 
    assert_raise(error){ @p.primes('z',6) }
 end

  def test_prime
    assert(@p.prime?(2017))
    assert_equal(false, @p.prime?(1))
    assert_equal(true, @p.prime?(11))
    assert_equal(false, @p.prime?(0))
  end

  def test_primes
    assert_equal([2,3,5],@p.primes(2,6))
    assert_equal([2,3,5],@p.primes(6,2))
  end
end
