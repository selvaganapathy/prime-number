class PrimeInterface
  def typecheck(n)
    return false unless n.is_a? Fixnum
    true
  end

  def not_a_number
    "Invalid/Not a Number" 
  end
end

class PrimeCheck < PrimeInterface
  def prime?(number)
    if typecheck(number)
      return false if [0,1].include? number
      (2...number).each{|divisor| return false if number % divisor == 0} 
      true
    else
      raise RuntimeError 
    end
  end
end

class PrimeList < PrimeCheck
  def primes(n,m)
    if typecheck(n) && typecheck(m)
      n,m = [n,m].sort
      (n..m).select(&method(:prime?))
    else
      raise RuntimeError
    end
  end
end

pc = PrimeCheck.new
pl = PrimeList.new
n,m = ARGV[0].to_i,ARGV[1].to_i
p = PrimeInterface.new
puts "#{n}.prime? => #{pc.prime?(n)}"
puts "#{m}.prime? => #{pc.prime?(m)}"
puts "primes(#{n},#{m}) => #{pl.primes(n,m)}"