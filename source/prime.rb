class PrimeInterface
  # prime or not
  def prime?(number)
    raise unless number.is_a? Fixnum
    return false if [0,1].include? number
    (2...number).each{|divisor| return false if number % divisor == 0} 
    true
  end
  
  # primes between range
  def primes(n,m)
    raise unless(n.is_a?(Fixnum) and m.is_a?(Fixnum))
    n,m = [n,m].sort
    (n..m).select(&method(:prime?))
  end
end

n,m = ARGV[0].to_i,ARGV[1].to_i
p = PrimeInterface.new
puts "#{n}.prime? => #{p.prime?(n)}"
puts "#{m}.prime? => #{p.prime?(m)}"
puts "primes(#{n},#{m}) => #{p.primes(n,m)}"
